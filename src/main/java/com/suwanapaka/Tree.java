package com.suwanapaka;

public class Tree {
    private char symbol;
    private int x;
    private int y;

    public Tree(int x, int y) {
        this.symbol = 'T';
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public void println() {
        System.out.println("Tree " + this.symbol + "(" + x + "," + y + ")");
    }

    public boolean ison(int x, int y) {
        return this.x == x && this.y ==y;
    }

    public void print() {
    }

}
